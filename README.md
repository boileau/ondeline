# Ondeline: a demonstration of how to solve the 2D linear wave propagation with Numpy or OpenCL

## Install

```
pip install -r requirements.txt
```

## Usage

Type `ondeline -h` to get help:

```
NAME
    ondeline - Run a linear wave propagation simulation in 2D

SYNOPSIS
    ondeline <flags>

DESCRIPTION
    Run a linear wave propagation simulation in 2D

FLAGS
    --n=N
        Type: int
        Default: 256
        Grid resolution
    --L=L
        Type: float
        Default: 1.0
        Grid lenght
    --tmax=TMAX
        Type: float
        Default: 1.0
        Simulation time
    --kernel=KERNEL
        Type: str
        Default: 'numpy'
        Kernel type: value are 'numpy', 'opencl' or 'compare'
    --animate=ANIMATE
        Type: bool
        Default: False
        Animate plotting during runtime
    --print_source=PRINT_SOURCE
        Type: bool
        Default: False
        Print OpenCL kernel source
    --pick_sources=PICK_SOURCES
        Type: bool
        Default: False
        Set source term locations using mouse clicks
    --plot=PLOT
        Type: bool
        Default: False
        plot solution at end of simulation
    --device_type=DEVICE_TYPE
        Type: str
        Default: 'GPU'
        Device type ('CPU' or 'GPU') for OpenCL kernel
    --iteplot=ITEPLOT
        Type: int
        Default: 40
        Number of iterations between two plots
```

Examples of CLI usage:

```
ondeline --plot  # run using numpy kernel (default) and plot last solution
ondeline --kernel opencl -a  # animate an opencl run
ondeline --kernel opencl -n 512  # run with opencl on a 512x512 grid
```

For example, the command:

```bash
ondeline -k opencl -a --pick_sources --plot -i 20
```

produces the following execution:

<img src="capture.gif" alt="screen capture" width="800"/>
