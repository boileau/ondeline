#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Resolution of a wave equation by the leap frog method
on regular grid
"""

import pyopencl as cl
import numpy as np
import fire
from pathlib import Path
import copy

from utils import Figure, Timer


class Ondeline():
    """An abstract class for a wave simulation"""

    compute_type = ''
    figname = ''
    c = np.sqrt(2.)  # sound speed
    cfl = 0.4  # time stepping

    def __init__(self, nx=256, ny=256, Lx=1., Ly=1., tmax=2.,
                 animate=False, precision='single', iteplot=100,
                 pick_sources=False,
                 **kwargs):

        self.nx = nx
        self.ny = ny
        self.Lx = Lx
        self.Ly = Ly
        self.tmax = tmax
        self.animate = animate
        self.precision = precision
        self.iteplot = iteplot
        self.pick_sources = pick_sources

        self.dx = self.Lx / (self.nx - 1)
        self.dy = self.Ly / (self.ny - 1)

        # For plotting
        self.x = np.linspace(0., self.Lx, num=self.nx)
        self.y = np.linspace(0., self.Ly, num=self.ny)

        # Timestep
        self.dt = self.cfl * np.sqrt(self.dx * self.dx + self.dy * self.dy) \
            / self.c

        self.np_real = np.float64 if self.precision == 'double' else np.float32

        self.t = 0.
        self.ite = 0
        self.ite_title = ''
        self.fig = None
        self.elapsed = 0.
        self.timer = Timer()

    def get_ite_title(self):
        """Return a formatted string giving iteration information"""
        self.elapsed = self.timer.elapsed()
        return \
            f"ite = {self.ite}, t = {self.t:f}, elapsed (s) = {self.elapsed:f}"

    def init_figure(self):
        """Initialize a figure with title and levels"""
        title = fr"$n_x = {self.nx}, n_y = {self.ny}$"
        self.fig = Figure(title=title,
                          levels=np.linspace(-4e-4, 4e-4, 64))

    def update_figure(self):
        """Update figure with data and title"""
        self.timer.stop()
        urange = f", {self.uplot.min():f}--{self.uplot.max():f}"
        self.fig.update(self.x, self.y, self.uplot,
                        suptitle=self.ite_title + urange,
                        cb=(self.ite == 0 and not self.pick_sources))
        self.timer.start()

    def ite_print(self):
        """Display figure or only print ite_title"""
        self.ite_title = self.get_ite_title()
        if self.animate:
            if self.ite % self.iteplot == 0:
                self.update_figure()
        else:
            print(self.ite_title, end='\r')

    def _start_message(self):
        return f"\nStart {self.compute_type} computation"

    def init_time_loop(self):
        """Just before starting time loop"""
        if self.animate and self.fig is None:
            self.init_figure()
        print(f"{self._start_message()}...")
        self.timer.start()

    def end_time_loop(self):
        """Just after time iteration"""
        self.timer.stop()
        self.elapsed = self.timer.elapsed()
        print()  # Print an empty line after self.ite_title last print
        if self.fig:
            self.fig.close()

    def get_figtitle(self):
        """Format figure title"""
        return f"{self.compute_type} solution"

    def plot(self, figname=None, show=False):
        """Plot and save figure"""
        self.fig = Figure(title=r"$p$", background=True)
        figtitle = self.get_figtitle()
        full_title = f"{figtitle} at t = {self.t:.4f}"
        self.fig.update(self.x, self.y, self.uplot, cb=True, show=False,
                        suptitle=full_title)
        if not figname:  # Use class figname if not passed as argument
            figname = self.figname
        filename = f"{figname}.png"
        print(f"Saving figure in {filename}")
        self.fig.save(filename)
        if show:
            self.fig.show()

    def __eq__(self, other):
        """Compare two simulations solutions"""
        return np.linalg.norm(self.uplot - other.uplot, ord=2) == 0

    def __sub__(self, other):
        """
        Return a new Ondeline object with uplot = self.uplot - other.uplot
        """
        diff = copy.deepcopy(self)  # Create a deepcopy of self
        diff.compute_type = \
            f"Difference between {self.compute_type} and {other.compute_type}"
        diff.figname = 'difference'
        diff.uplot = self.uplot - other.uplot
        return diff

    def __deepcopy__(self, memo):
        """Implement deepcopy as a copy of constructor arguments and uplot"""
        newcopy = Ondeline(nx=self.nx, ny=self.ny, Lx=self.Lx, Ly=self.Ly,
                           tmax=self.tmax, animate=self.animate,
                           precision=self.precision, iteplot=self.iteplot)
        newcopy.uplot = self.uplot
        newcopy.t = self.t
        newcopy.ite = self.ite
        return newcopy

    def norm(self, order=2):
        """Return norm (default L2)"""
        return np.linalg.norm(self.uplot, ord=order)

    def solve(self):
        """Solve for time"""
        pass

    def get_sources(self):
        """get source coordinates"""
        if self.pick_sources:
            # Pickup source coordinates using mouse clicks
            self.init_figure()
            self.fig.update(self.x, self.y, self.uplot,
                            suptitle=("Left-click to add a source point. "
                                      "Right-click to run simulation"),
                            cb=True)
            return np.array(self.fig.pick_sources())
        else:
            # Default source location
            return np.array(((0.25, 0.25),))


class NumpyOndeline(Ondeline):
    """A wave simulation using Numpy"""

    compute_type = 'Numpy'
    figname = 'np_ondeline'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.u = np.zeros(shape=(3, self.nx+2, self.ny+2), dtype=self.np_real)
        self.uplot = self.u[1, 1:-1, 1:-1]  # A view of u
        self.xs = self.get_sources()

    def solve(self):
        """Solve for time"""

        u = self.u
        # beta
        beta_x = self.c * self.dt / self.dx
        beta_y = self.c * self.dt / self.dy
        r = 0.
        r_star = beta_x * (1 - r) / (1 + r)  # For reflection conditions

        # For source term
        r = self.Lx / 10.
        xx, yy = np.meshgrid(self.x, self.y)
        mask = np.zeros_like(xx)
        # Loop on source terms coordinates
        for xm, ym in self.xs[:]:
            d = np.sqrt((xx - xm)**2 + (yy - ym)**2)
            mask += (d < r) * self.dt * self.dt

        def source_term(t):
            """Apply a sinus time function as local source"""
            return mask * np.sin(10 * 2 * np.pi * np.sqrt(2) * self.c * t)

        def reflection_2d():
            """Apply reflection condition to all boundaries"""

            def reflection(u_b, u_in, beta):
                """Apply reflection condition to a boundary"""
                u_b[2] = 1 / (1 + r_star) * ((r_star - 1) * u_b[0]
                                             + 2 * beta ** 2
                                             * (u_in[1] - u_b[1])
                                             + 2 * u_b[1])

            # x = 0 boundary
            reflection(u[:, 1, :], u[:, 2, :], beta_x)

            # x = lx boundary
            reflection(u[:, -2, :], u[:, -3, :], beta_x)

            # y = 0 boundary
            reflection(u[:, :, 1], u[:, :, 2], beta_y)

            # y = ly boundary
            reflection(u[:, :, -2], u[:, :, -3], beta_y)

        self.init_time_loop()

        while self.t < self.tmax:
            self.t += self.dt

            # Leapfrog scheme
            u[2, 1:-1, 1:-1] = 2*(1 - beta_x**2 - beta_y**2)*u[1, 1:-1, 1:-1] \
                + beta_x**2*(u[1, :-2, 1:-1] + u[1, 2:, 1:-1]) \
                + beta_y**2*(u[1, 1:-1, :-2] + u[1, 1:-1, 2:]) \
                - u[0, 1:-1, 1:-1]

            s = source_term(self.t)
            u[2, 1:-1, 1:-1] -= s

            reflection_2d()

            # Update solution and time
            u[0] = u[1]
            u[1] = u[2]

            self.ite_print()

            self.ite += 1

        self.end_time_loop()


class OpenCLOndeline(Ondeline):
    """A wave simulation using OpenCL"""

    compute_type = 'OpenCL'
    figname = 'ocl_ondeline'

    def __init__(self, *args, **kwargs):
        self.print_source = kwargs.pop('print_source', False)
        self.device_type = kwargs.pop('device_type', 'GPU')
        super().__init__(*args, **kwargs)
        # host array
        self.h_un = np.zeros(self.nx * self.ny, dtype=self.np_real)
        # a view of h_un for plotting :
        self.uplot = np.reshape(self.h_un, (self.nx, self.ny))
        self.xs = self.get_sources()

        # Kernel path is relative to module directory
        self.kernel_file = Path(__file__).resolve().parent / "ondeline.cl"
        self.kernel_source = self.load_kernel()
        self.kernel_elapsed = 0.
        self.device_name = ''

    def load_kernel(self):
        """Return kernel source using defined parameters dictionary"""

        FF = "_F"  # string to be replaced in OpenCL kernel source

        def stringify(parameter):
            """Return an exponential-formatted string from float"""
            if type(parameter) is float:
                return f'({parameter:e}{FF})'
            elif type(parameter) is np.ndarray:
                return str(
                    parameter.tolist()).replace('[', '{').replace(']', '}')
            else:
                return f'({parameter})'

        with open(self.kernel_file, "r") as f:
            source = f.read()

        # Parameters in #define header of kernel_file
        parameters = {'nx': self.nx,
                      'ny': self.ny,
                      'dx': float(self.dx),
                      'dy': float(self.dy),
                      'dt': float(self.dt),
                      'c': float(self.c),
                      'xs': self.xs,
                      'nxs': self.xs.shape[0]
                      }
        for k, v in parameters.items():
            source = source.replace(f'_{k}_', stringify(v))

        if self.precision == "double":
            source = source.replace(FF, "")
            source = source.replace("_real_", "double")
        else:
            source = source.replace(FF, "f")
            source = source.replace("_real_", "float")

        if self.print_source:
            print(source)

        return source

    def get_ite_title(self):
        """Return a formatted string giving iteration information"""
        self.elapsed = self.timer.elapsed()
        return f"ite = {self.ite}, t = {self.t:f}, "\
               + f"elapsed (s) = {self.elapsed:f}" \
               + f" (kernel (s) = {self.kernel_elapsed:f})"

    def get_figtitle(self):
        """Format figure title"""
        return f"{self.compute_type} solution on {self.device_name}"

    def update_figure(self):
        """Update figure with data and title"""
        cl.enqueue_copy(self.queue, self.h_un, self.d_un).wait()
        super().update_figure()

    @staticmethod
    def get_context(device_type='GPU'):
        """
        Return a context corresponding to the first device_type
        ('GPU' or 'CPU') found
        """
        for platform in cl.get_platforms():
            try:
                ctx = cl.Context(
                    dev_type=getattr(cl.device_type, device_type),
                    properties=[(cl.context_properties.PLATFORM, platform)])
                return ctx
            except cl.RuntimeError as e:
                pass

    def _start_message(self):
        return f"{super()._start_message()} on {self.device_type}"

    def solve(self):
        """Solve for time"""

        # OpenCL init
        ctx = self.get_context(self.device_type)
        self.device_name = ctx.devices[0].name
        mf = cl.mem_flags

        # compile OpenCL C program
        prg = cl.Program(ctx, self.kernel_source).build()

        # set the type of tnow in kernel
        kernel_time_step = prg.time_step
        kernel_time_step.set_scalar_arg_dtypes([self.np_real,
                                                None, None, None])

        # create OpenCL buffers
        buffer_size = self.nx * self.ny * np.dtype(self.np_real).itemsize
        self.d_un = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
        d_unp1 = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
        d_unm1 = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)

        # create a queue (for submitting opencl operations)
        self.queue = cl.CommandQueue(ctx,
                                     properties=cl.command_queue_properties.
                                     PROFILING_ENABLE)

        # set initial solution
        event = prg.init_sol(self.queue, (self.nx * self.ny, ), None,
                             self.d_un, d_unm1)
        event.wait()

        self.init_time_loop()

        while self.t < self.tmax:
            self.t += self.dt

            # execute kernel
            event = kernel_time_step(self.queue, (self.nx * self.ny, ), None,
                                     self.t, d_unm1, self.d_un, d_unp1)
            event.wait()
            self.kernel_elapsed += 1e-9 * (event.profile.end
                                           - event.profile.start)

            # exchange buffer references for avoiding a copy
            d_unm1, self.d_un, d_unp1 = self.d_un, d_unp1, d_unm1

            self.ite_print()

            self.ite += 1

        # copy OpenCL data to host and return the results
        cl.enqueue_copy(self.queue, self.h_un, self.d_un).wait()
        self.queue.finish()

        self.end_time_loop()


def run(n: int = 256, L: float = 1.0, tmax: float = 1.0,
        kernel: str = 'numpy', device_type: str = 'GPU',
        print_source: bool = False,
        animate: bool = False,
        plot: bool = False,
        iteplot: int = 40,
        pick_sources: bool = False):
    """
    Run a linear wave propagation simulation in 2D

    Parameters
    ----------
    n :
        Grid resolution
    L :
        Grid lenght
    tmax:
        Simulation time
    kernel:
        Kernel type: value are 'numpy', 'opencl' or 'compare'
    device_type:
        Device type ('CPU' or 'GPU') for OpenCL kernel
    print_source:
        Print OpenCL kernel source
    animate:
        Animate plotting during runtime
    iteplot:
        Number of iterations between two plots
    plot:
        plot solution at end of simulation
    pick_sources:
        Set source term locations using mouse clicks
    """

    params = dict(nx=n, ny=n, Lx=L, Ly=L, tmax=tmax,
                  animate=animate, print_source=print_source,
                  pick_sources=pick_sources,
                  iteplot=iteplot)

    if kernel in ('numpy', 'compare'):
        # Solving Ondeline with Numpy
        np_onde = NumpyOndeline(**params)
        np_onde.solve()
        np_onde.plot(show=((kernel != 'compare') and plot))
        print(((kernel != 'compare') and plot))

    if kernel in ('opencl', 'compare'):
        # Solving Ondeline with OpenCL
        cl_onde = OpenCLOndeline(device_type=device_type, **params)
        cl_onde.solve()
        cl_onde.plot(show=((kernel != 'compare') and plot))

    if kernel == 'compare':
        # Checking difference
        if np_onde == cl_onde:
            print("np_onde == cl_onde")
        else:
            print("np_onde != cl_onde")
            print("L2 norm np_onde =", np_onde.norm())
            print("L2 norm cl_onde =", cl_onde.norm())
            diff_onde = np_onde - cl_onde
            print("L2 error =", diff_onde.norm())
            diff_onde.plot(show=True)
        cl_onde.plot(show=plot)


def main():
    fire.Fire(run)


if __name__ == '__main__':
    main()
