"""A Figure class to plot animations and final plots"""

from matplotlib import rcParams
import matplotlib.pyplot as plt
from matplotlib.backend_bases import MouseButton
from timeit import default_timer


class Timer:
    """A very simple timer"""

    def __init__(self):
        self.started = False
        self.start_time = 0.
        self.exec_time = 0.

    def start(self):
        self.started = True
        self.start_time = default_timer()

    def stop(self):
        if self.started:
            self.exec_time += default_timer() - self.start_time
        self.started = False

    def elapsed(self):
        return default_timer() - self.start_time

    def __repr__(self):
        return "Elapsed time [s] = {:f}".format(self.exec_time)


class Figure:
    """A 2D colormap figure using contourf"""

    def __init__(self, title='', levels=64, figsize=(10, 8), background=False):
        if background:
            rcParams['backend'] = "nbAgg"
        self.fig, self.ax = plt.subplots(figsize=figsize)
        self.ax.set_xlabel(r"$x$")
        self.ax.set_ylabel(r"$y$")
        self.ax.set_aspect('equal')
        self.ax.set_title(title)
        self.levels = levels

    def _draw(self):
        """Draw canvas according to backend"""
        self.fig.canvas.flush_events()
        if rcParams['backend'] == "nbAgg":
            self.fig.canvas.draw()
        else:
            plt.pause(1e-6)

    def update(self, x, y, wplot, suptitle='', cb=False, show=False):
        """Update figure with data"""
        self.fig.suptitle(suptitle)
        # Empty old plot before new plot
        for collection in plt.gca().collections:
            plt.gca().collections.remove(collection)
        ctf = self.ax.contourf(x, y, wplot, self.levels)
        if cb:
            self.fig.colorbar(ctf, ax=self.ax, format='%.2g')
        self._draw()
        if show:
            plt.show()

    def pick_sources(self):
        """Pickup source locations"""

        source_coordinates = []

        def on_click(event):
            if event.inaxes and event.button == MouseButton.LEFT:
                x, y = event.xdata, event.ydata
                source_coordinates.append([x, y])
                self.ax.plot(x, y, 'ro')
                self._draw()
                self.fig.waitforbuttonpress()
            elif event.button == MouseButton.RIGHT:
                self.fig.canvas.mpl_disconnect(cid)

        cid = self.fig.canvas.mpl_connect('button_press_event', on_click)
        plt.show(block=False)
        self.fig.waitforbuttonpress()
        return source_coordinates

    def save(self, *args, **kwargs):
        self.fig.savefig(*args, **kwargs)

    @staticmethod
    def show():
        plt.show()

    def close(self):
        plt.close(self.fig)
